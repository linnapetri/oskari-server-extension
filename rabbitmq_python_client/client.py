import pika
import os

host = os.environ.get('MQ_HOST', 'localhost')
port = os.environ.get('MQ_PORT', '57621')


# Connect
connection = pika.BlockingConnection(pika.ConnectionParameters(host, port, connection_attempts=999))
channel = connection.channel()

channel.queue_declare(queue='lidar_requests', durable=True)
channel.queue_declare(queue='segment_requests', durable=True)

# Receive messages

def callbackLidar(ch, method, properties, body):
    print("LIDAR Received: %r" % body)

def callbackSegment(ch, method, properties, body):
    print("SEGMENT Received: %r" % body)



channel.basic_consume(queue='lidar_requests',
                      auto_ack=True,
                      on_message_callback=callbackLidar)

channel.basic_consume(queue='segment_requests',
                      auto_ack=True,
                      on_message_callback=callbackSegment)

print("Waiting for messages. Press CTRL+C to close.")
try:
    channel.start_consuming()
except KeyboardInterrupt:
    pass

# Close

connection.close()
print("Connection closed")
