# Peltodata application update guide

This file contains main steps, for more details check same steps from PELTODATA_INSTALLATION 

1. Pull changes from server-extension and peltodata-frontend
2. compile server
3. copy oskari-map.war to jetty webapps 
4. pull changes from frontend
5. compile frontend
6. override oskari-server/peltodata/dist with built dist contents
7. check for missing configuration changes

