# cache maven
FROM maven:3.5.3-jdk-8-alpine as builder
WORKDIR /build
COPY pom.xml .
COPY peltodata-module/pom.xml peltodata-module/pom.xml
RUN mvn -pl peltodata-module dependency:go-offline
# Allow error because peltodata.module fails. Still downloads cache which is correct

# full build
COPY . .
RUN mvn -DskipTests=true package

FROM jetty:9-jdk8 as run
USER root
RUN apt-get update && apt-get install -y nano gdal-bin python3-pip
#RUN sed -i 's/debian buster main/debian unstable main/' /etc/apt/sources.list
#RUN apt-get update && apt-get install -y gdal-bin python3-pip libgcc-8-dev-
RUN pip3 install pillow numpy
RUN mkdir -p /opt/oskaridata/userupload && chown -R jetty:jetty /opt/oskaridata/userupload
USER jetty
COPY --from=builder /build/peltodata-module/target/oskari-map.war /var/lib/jetty/webapps
# COPY webapp-map/target/oskari-map.war /var/lib/jetty/webapps
COPY files/jetty /var/lib/jetty
COPY files/copy-file.py /opt/oskari/copy-file.py
EXPOSE 8080
CMD ["java", "-jar", "/usr/local/jetty/start.jar"]

