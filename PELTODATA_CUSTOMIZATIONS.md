# List of modified Oskari files and reasons for modifications

###SaveLayerHandler.java
is overriden because we want to call saveLayer from our code with custom ActionParameters (without HttpServletRequest). 
Changes are:
- SaveResult is made public along with getters and setters
- saveLayer method is made public
- All ActionParameters.getRequest() calls are converted to use straight calls from ActionParameters. so we can override it with our own (InsertLayerActionParams)

###ActionParameters.java
basically added getHttpParamNames method which calls getRequest().get... 
   
