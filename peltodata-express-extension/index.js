const express = require('express')
const reportmaker = require('./report-maker');
const app = express()
const port = 3000

app.get('/:folder', async (req, res) => {
  const folder = req.params.folder;
  console.log(folder);
  const report = await reportmaker(folder);
  res.send(report);
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
