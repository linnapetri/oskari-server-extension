alter table peltodata_field
    add lohko_id int;

alter table peltodata_field alter column crop_type drop not null;

alter table peltodata_field alter column crop_type drop default;

alter table peltodata_field alter column sowing_date drop not null;

alter table peltodata_field alter column sowing_date drop default;

alter table peltodata_field alter column farm_id drop default;

