package fi.peltodata;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import fi.nls.oskari.log.LogFactory;
import fi.nls.oskari.log.Logger;
import fi.nls.oskari.service.ServiceException;
import fi.peltodata.service.PeltodataService;
import fi.peltodata.service.PeltodataServiceMybatisImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@Configuration
public class PeltodataConfig {
    public static final String PROP_EXPRESS_EXTENSION_URL = "peltodata.express.extension.url";
    private static Logger logger = LogFactory.getLogger(PeltodataConfig.class);

    public static final String PROP_RABBITMQ_ADDRESS = "peltodata.rabbitmq.address";
    public static String PROP_UPLOAD_ROOT_DIR_PATH = "peltodata.upload.root.dir";
    public static String PROP_UPLOAD_JSON_DIR_PATH = "peltodata.json.dir";

    @Bean
    public PeltodataService peltodataService() throws ServiceException {
        return new PeltodataServiceMybatisImpl();
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return mapper;
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver resolver = new CommonsMultipartResolver();
        resolver.setMaxUploadSize(1000000000000L);
        resolver.setMaxUploadSizePerFile(1000000000000L);
        return resolver;
    }
}
