package fi.peltodata.controller.response;

import java.io.Serializable;

public class UserFarmResponse implements Serializable {
    private String farmId;

    public UserFarmResponse(String farmId) {
        this.farmId = farmId;
    }

    public UserFarmResponse() {
    }


    public String getFarmId() {
        return farmId;
    }

    public void setFarmId(String farmId) {
        this.farmId = farmId;
    }
}
