package fi.peltodata.service;

import fi.nls.oskari.annotation.Oskari;
import fi.nls.oskari.domain.User;
import fi.nls.oskari.domain.map.OskariLayer;
import fi.nls.oskari.log.LogFactory;
import fi.nls.oskari.log.Logger;
import fi.nls.oskari.map.layer.OskariLayerService;
import fi.nls.oskari.map.layer.OskariLayerServiceMybatisImpl;
import fi.nls.oskari.service.OskariComponent;
import fi.nls.oskari.service.ServiceException;
import fi.nls.oskari.user.MybatisUserService;
import fi.nls.oskari.util.PropertyUtil;
import fi.peltodata.domain.Farmfield;
import fi.peltodata.domain.FarmfieldFile;
import fi.peltodata.domain.FarmfieldFileDataType;
import fi.peltodata.domain.Peltolohko;
import fi.peltodata.execution.FarmTaskMessage;
import fi.peltodata.execution.RabbitMqQueueServiceImpl;
import fi.peltodata.execution.TaskMessage;
import fi.peltodata.repository.PeltodataRepository;
import fi.peltodata.repository.PeltodataRepositoryImpl;
import org.oskari.service.maplayer.OskariMapLayerGroupService;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Oskari
public class PeltodataServiceMybatisImpl extends OskariComponent implements PeltodataService {
    private static final Logger LOG = LogFactory.getLogger(PeltodataRepositoryImpl.class);
    private final RabbitMqQueueServiceImpl queueService;
    private PeltodataRepository peltodataRepository;
    private OskariLayerService oskariLayerService;
    private MybatisUserService userService;

    public PeltodataServiceMybatisImpl() throws ServiceException {
        this(new MybatisUserService());
    }

    private PeltodataServiceMybatisImpl(MybatisUserService userService) {
        this.oskariLayerService = new OskariLayerServiceMybatisImpl();
        this.userService = userService;
        this.peltodataRepository = new PeltodataRepositoryImpl(userService, new OskariLayerServiceMybatisImpl());
        this.queueService = new RabbitMqQueueServiceImpl();
    }

    @Override
    public Farmfield findFarmfield(long id) {
        return peltodataRepository.findFarmfield(id);
    }

    @Override
    public List<Farmfield> findAllFarmfields() {
        return peltodataRepository.findAllFarmFields();
    }

    @Override
    public List<Farmfield> findAllFarmfieldsByUser(long userId) {
        return peltodataRepository.findAllFarmfieldsByUser(userId);
    }

    @Override
    public boolean farmfieldBelongsToUser(long farmfieldId, long userId) {
        return peltodataRepository.farmfieldBelongsToUser(farmfieldId, userId);
    }

    @Override
    public void updateFarmfield(final Farmfield farmfield) {
        peltodataRepository.updateFarmfield(farmfield);
    }

    private Map<String, String> getLanguageNameMap() {
        Set<String> languages = new HashSet<>();
        languages.add(PropertyUtil.getDefaultLanguage());
        languages.addAll(Arrays.asList(PropertyUtil.getSupportedLanguages()));
        Map<String, String> names = new HashMap<>();
        for (String language : languages) {
            names.put(language, "");
        }
        return names;
    }

    @Override
    public synchronized long insertFarmfield(final Farmfield farmfield) {
        long farmfieldId = peltodataRepository.insertFarmfield(farmfield);
        // farmfield created ok, next check if group and dataprovider exists
        User user = farmfield.getUser();
        if (user == null) {
            Long userId = farmfield.getUserId();
            user = userService.find(userId);
            if (user == null) {
                throw new RuntimeException("Farm created but failed get user");
            }
            farmfield.setUser(user);
        }

        return farmfieldId;
    }

    @Override
    public void deleteFarmfield(long farmfieldId) {
        Farmfield farmfield = peltodataRepository.findFarmfield(farmfieldId);
        this.deleteFarmfield(farmfield);
    }

    @Override
    public void deleteFarmfield(Farmfield farmfield) {
        if (farmfield == null || farmfield.getId() == null) {
            throw new IllegalArgumentException("farmfield or farmfieldid is null");
        }

        farmfield.getLayers().stream().map(OskariLayer::getId).forEach(oskariLayerService::delete);
        peltodataRepository.deleteFarmfieldFilesForField(farmfield.getId());
        peltodataRepository.deleteFarmfield(farmfield.getId());
    }

    @Override
    public List<String> findAllFarmfieldFiles(long farmfieldId) {
        try {
            Path farmfieldRootPath = Paths.get(FileService.getFarmUploadRootPath(farmfieldId));
            if (Files.exists(farmfieldRootPath)) {
                List<String> fileList = Files.walk(Paths.get(FileService.getFarmUploadRootPath(farmfieldId)))
                        .filter(Files::isRegularFile)
                        .map(FileService::getRelativeFarmfieldFilePath)
                        .collect(Collectors.toList());
                return fileList;
            }
        } catch (IOException e) {
            LOG.error(e, "Error while traversing path: " + FileService.getFarmUploadRootPath(farmfieldId));
        }
        return Collections.emptyList();
    }

    @Override
    public FarmfieldFile uploadLayerData(long farmfieldId, InputStream inputStream, FarmfieldFileDataType dataType, String originalFilename, String filename) {
        int year = Year.now().getValue();
        String uploadPath = FileService.getUploadPath(year, farmfieldId, dataType);
        String filePathString = uploadPath + filename;
        LOG.info("about upload file : " + filePathString);
        boolean success = false;
        Path newFile = Paths.get(filePathString);
        String fullPath = FileSystems.getDefault().getPath(newFile.toString()).normalize().toAbsolutePath().toString();
        LOG.info("full path " + fullPath);
        try {
            Files.createDirectories(newFile.getParent());
            Files.copy(inputStream, newFile);
            FarmfieldFile farmfieldFile = new FarmfieldFile();
            farmfieldFile.setFarmfieldId(farmfieldId);
            farmfieldFile.setFileDate(LocalDate.now());
            farmfieldFile.setOriginalFilename(originalFilename);
            farmfieldFile.setFullPath(fullPath);
            farmfieldFile.setType(dataType.getTypeId());
            long id = peltodataRepository.insertFarmfieldFile(farmfieldFile);
            LOG.info("inserted " + farmfieldFile + " " + id);
            FarmfieldFile ret = peltodataRepository.findFarmfieldFile(id);
            return ret;
        } catch (IOException e) {
            LOG.error(e, "Error occured while writing file: " + filePathString);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void createFarmfieldLayer(long farmfieldId, long farmfieldFileId,
                                     FarmfieldFileDataType inputDataType, FarmfieldFileDataType outputDataType, User user) {
        LOG.info("createFarmfieldLayer id={} inputfile={} inputtype={} outputtype={}",
                farmfieldId, farmfieldFileId, inputDataType.toString(), outputDataType.toString());
        FarmfieldFile farmfieldFile = peltodataRepository.findFarmfieldFile(farmfieldFileId);
        Farmfield farmfield = findFarmfield(farmfieldId);

        if (farmfieldId != farmfieldFile.getFarmfieldId()) {
            throw new RuntimeException("invalid farmfield for file");
        }
    }

    @Override
    public String getInputFilename(FarmfieldFileDataType dataType) {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")) + "__" + dataType.getFolderName() + "." + dataType.getDataFormat();
    }

    @Override
    public boolean farmfieldFileBelongsToFarmAndUser(Long fileId, Long farmFieldId, User user) {
        Farmfield farmfield = peltodataRepository.findFarmfield(farmFieldId);
        FarmfieldFile file = peltodataRepository.findFarmfieldFile(fileId);
        return file.getFarmfieldId() == farmfield.getId() && user.getId() == farmfield.getUserId();
    }

    @Override
    public FarmfieldFile findFarmfieldFile(Long fileId) {
        return peltodataRepository.findFarmfieldFile(fileId);
    }

    @Override
    public void updateFarmfieldFile(FarmfieldFile file) {
        peltodataRepository.updateFarmfieldFile(file);
    }

    @Override
    public boolean farmIdExists(String farmId) {
        return peltodataRepository.farmIdExists(farmId);
    }

    @Override
    public List<Peltolohko> getPeltolohkot(String farmId) {
        return peltodataRepository.getPeltolohkot(farmId);
    }

    @Override
    public void registerFarmId(String farmId, User user) {
        try {
            List<Peltolohko> peltolohkot = getPeltolohkot(farmId);
            for (Peltolohko peltolohko : peltolohkot) {
                Farmfield field = new Farmfield();
                field.setUser(user);
                field.setFarmId(farmId);
                field.setLohkoId(peltolohko.getGid());
                field.setCropType("");
                field.setDescription(peltolohko.getLohko());
                insertFarmfield(field);
            }

            TaskMessage taskMessage = new FarmTaskMessage(farmId);
            queueService.addTaskToQueue(taskMessage);
        } catch (Exception e) {
            LOG.error("error when creating lidar layers", e);
        }
    }

    @Override
    public void changeFarmId(String farmId, User user) {
        List<Farmfield> fields = findAllFarmfieldsByUser(user.getId());

        if (fields.size() > 0 && !user.hasRole("Adviser")) {
            throw new RuntimeException("user cannot edit farmid without correct role");
        }

        for (Farmfield field : fields) {
            deleteFarmfield(field);
        }

        registerFarmId(farmId, user);
    }
}
