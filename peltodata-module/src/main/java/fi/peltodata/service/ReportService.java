package fi.peltodata.service;

import fi.nls.oskari.util.PropertyUtil;
import fi.peltodata.PeltodataConfig;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;

public class ReportService {
    public String getReport(String farmId) {
        String expressExtensionUrl = PropertyUtil.get(PeltodataConfig.PROP_EXPRESS_EXTENSION_URL, "http://localhost:3000");

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(expressExtensionUrl + "/" + farmId);
        try {
            CloseableHttpResponse response1 = httpclient.execute(httpGet);
            HttpEntity e = response1.getEntity();
            return IOUtils.toString(e.getContent(), "UTF-8");
        } catch (IOException e) {
            throw new RuntimeException("failed to generate field report", e);
        }
    }
}
