package fi.peltodata.service;

import fi.nls.oskari.domain.User;
import fi.peltodata.domain.*;

import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;

public interface PeltodataService {
    Farmfield findFarmfield(long id);
    List<Farmfield> findAllFarmfields();
    List<Farmfield> findAllFarmfieldsByUser(long userId);
    boolean farmfieldBelongsToUser(long farmfieldId, long userId);
    long insertFarmfield(final Farmfield farmfield);
    void updateFarmfield(final Farmfield farmfield);
    void deleteFarmfield(final long layerId);
    void deleteFarmfield(Farmfield farmfield);

    FarmfieldFile uploadLayerData(long farmfieldId, InputStream inputStream, FarmfieldFileDataType dataType, String originalFilename, String filename);

    List<String> findAllFarmfieldFiles(long farmfieldId);

    void createFarmfieldLayer(long farmfieldId, long farmfieldFileId,
                              FarmfieldFileDataType inputDataType, FarmfieldFileDataType outputDataType, User user);

    String getInputFilename(FarmfieldFileDataType dataType);

    boolean farmfieldFileBelongsToFarmAndUser(Long fileId, Long farmFieldId, User user);

    FarmfieldFile findFarmfieldFile(Long fileId);

    void updateFarmfieldFile(FarmfieldFile file);

    boolean farmIdExists(final String farmId);

    List<Peltolohko> getPeltolohkot(final String farmId);

    void registerFarmId(String farmId, User user);

    void changeFarmId(String farmId, User user);
}
