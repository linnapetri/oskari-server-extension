package fi.peltodata.execution;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import fi.nls.oskari.util.PropertyUtil;
import fi.peltodata.PeltodataConfig;

import java.io.IOException;

public class RabbitMqClient {
    private static String rabbitMqAddress = PropertyUtil.get(PeltodataConfig.PROP_RABBITMQ_ADDRESS);
    // private static String rabbitMqAddress = "amqp://localhost:56721";

    private static Connection c = null;
    private static Channel channel;

    public synchronized static Connection getConnection() {
        if (c == null || !c.isOpen()) {
            initConnection();
        }

        return c;
    }

    public static Channel getChannel() {
        getConnection();
        return channel;
    }

    public static void close() {
        if (c != null && c.isOpen()) {
            try {
                c.close();
            } catch (IOException ignored) {
            }
        }
    }

    public static void initConnection() {
        ConnectionFactory cf = new ConnectionFactory();
        try {
            cf.setUri(rabbitMqAddress);
            Connection c = cf.newConnection();
            RabbitMqClient.c = c;
            Channel channel = c.createChannel();
            channel.queueDeclare("farm_requests", true, false, false, null);
            RabbitMqClient.channel = channel;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
