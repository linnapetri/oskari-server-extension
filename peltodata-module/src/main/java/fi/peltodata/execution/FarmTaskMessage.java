package fi.peltodata.execution;

public class FarmTaskMessage extends TaskMessage {
    private String farmId;
    public static String CHANNEL_NAME = "farm_requests";


    public FarmTaskMessage() {
        super(CHANNEL_NAME);
    }

    public FarmTaskMessage(String farmId) {
        super(CHANNEL_NAME);
        this.farmId = farmId;
    }

    public String getFarmId() {
        return farmId;
    }

    public void setFarmId(String farmId) {
        this.farmId = farmId;
    }
}
