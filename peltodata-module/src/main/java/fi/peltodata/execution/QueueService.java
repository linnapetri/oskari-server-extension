package fi.peltodata.execution;

import fi.peltodata.execution.TaskMessage;

public interface QueueService {
    void addTaskToQueue(TaskMessage task);
}
