package fi.peltodata;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fi.nls.oskari.service.ServiceException;
import fi.peltodata.service.ClipHelper;
import org.checkerframework.checker.units.qual.C;

import java.util.ArrayList;
import java.util.List;

public class GeoJson {
    private String type;
    private List<List<List<List<String>>>> coordinates;
    private Crs crs;

    public static void main(String[] args) throws JsonProcessingException, ServiceException {
        String json = "{\"type\":\"MultiPolygon\",\"coordinates\":[[[[296314.21115,6788473.095],[296323.18385,6788492.089],[296322.89675,6788488.383],[296337.5438,6788488.443],[296400.2108,6788513.755],[296404.8638,6788519.399],[296547.44195,6788588.458],[296538.38125,6788603.452],[296532.28615,6788608.297],[296526.03485,6788608.297],[296523.5351,6788598.452],[296518.8469,6788594.858],[296513.84685,6788589.545],[296508.06525,6788595.639],[296504.00185,6788603.609],[296498.3759,6788614.704],[296487.1251,6788621.579],[296472.90485,6788625.642],[296463.529,6788629.861],[296453.37215,6788630.643],[296461.1849,6788640.488],[296472.5919,6788658.302],[296504.15805,6788709.089],[296521.66015,6788734.248],[296532.4418,6788747.843],[296546.3502,6788763.782],[296552.75715,6788769.251],[296589.63575,6788755.812],[296625.57715,6788737.685],[296644.6418,6788726.591],[296661.98825,6788706.276],[296694.1792,6788661.271],[296719.1811,6788624.939],[296757.1542,6788557.276],[296778.4062,6788518.443],[296784.81315,6788506.411],[296786.2228,6788501.592],[296792.46695,6788493.375],[296762.40725,6788474.148],[296695.59105,6788430.343],[296693.7238,6788433.777],[296690.4282,6788439.841],[296651.36115,6788496.722],[296620.2482,6788538.5],[296604.7481,6788559.75],[296600.138,6788553.857],[296689.99315,6788430.419],[296685.35225,6788426.437],[296676.08805,6788419.732],[296662.14115,6788410.229],[296587.24985,6788362.538],[296525.46615,6788323.9],[296523.35525,6788322.609],[296469.27705,6788289.545],[296436.43985,6788326.379],[296429.87395,6788333.311],[296422.851,6788340.726],[296376.3419,6788395.477],[296336.33105,6788445.317],[296319.92125,6788466.783],[296318.60015,6788468.587],[296317.41105,6788470.211],[296314.21115,6788473.095]]]]}";

        ObjectMapper om = new ObjectMapper();
        GeoJson js = om.readValue(json, GeoJson.class);
        System.out.println(js.getUnwrappedCoordinates());

        List<GeoJson> geoJsons = new ArrayList<>();
        geoJsons.add(js);

        String param = ClipHelper.getClipParam(geoJsons);

        System.out.println(param);
        System.out.println(param.startsWith("&clip=MULTIPOLYGON(((296314"));
        System.out.println(param.endsWith("6788473.095)))"));
        System.out.println(param.contains("296314.21115 6788473.095,296323.18385 6788492.089"));

        geoJsons.add(js);
        String param2 = ClipHelper.getClipParam(geoJsons);
        System.out.println(param2);
        System.out.println(param2.startsWith("&clip=MULTIPOLYGON(((296314"));
        System.out.println(param2.endsWith("6788473.095)))"));
        System.out.println(param2.contains("6788473.095)),((296314.21115 6788473.095"));

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<List<String>> getUnwrappedCoordinates() {
        if (coordinates.size() > 0 && coordinates.get(0).size() > 0) {
            return coordinates.get(0).get(0);
        }

        return new ArrayList<>();
    }

    public List<List<List<List<String>>>> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<List<List<List<String>>>> coordinates) {
        this.coordinates = coordinates;
    }

    public Crs getCrs() {
        return crs;
    }

    public void setCrs(Crs crs) {
        this.crs = crs;
    }

    public static class Crs {
        private String type;
        private CrsProperties properties;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public CrsProperties getProperties() {
            return properties;
        }

        public void setProperties(CrsProperties properties) {
            this.properties = properties;
        }
    }

    public static class CrsProperties {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
