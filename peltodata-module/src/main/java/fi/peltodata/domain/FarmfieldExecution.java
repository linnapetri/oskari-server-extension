package fi.peltodata.domain;

import java.util.Date;

public class FarmfieldExecution {
    // 0 = started, 10 = success, -10 = error
    private Long id;
    private int state;
    private String outputType;
    private Date executionStartedAt;
    private Long farmfieldId;
    private Long farmfieldFileId;
    private String outputFilename;

    public Long getFarmfieldFileId() {
        return farmfieldFileId;
    }

    public void setFarmfieldFileId(Long farmfieldFileId) {
        this.farmfieldFileId = farmfieldFileId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getOutputType() {
        return outputType;
    }

    public void setOutputType(String outputType) {
        this.outputType = outputType;
    }

    public Date getExecutionStartedAt() {
        return executionStartedAt;
    }

    public void setExecutionStartedAt(Date executionStartedAt) {
        this.executionStartedAt = executionStartedAt;
    }

    public Long getFarmfieldId() {
        return farmfieldId;
    }

    public void setFarmfieldId(Long farmfieldId) {
        this.farmfieldId = farmfieldId;
    }

    public String getOutputFilename() {
        return outputFilename;
    }

    public void setOutputFilename(String outputFilename) {
        this.outputFilename = outputFilename;
    }
}
