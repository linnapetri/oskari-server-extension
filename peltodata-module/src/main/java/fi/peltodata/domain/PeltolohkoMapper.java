package fi.peltodata.domain;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

public interface PeltolohkoMapper {
    @Select("select gid,ely,knro,tiltu,lohko,pinta_ala,ymparys,st_asgeojson(geom) as geojson from peltodata_peltolohkot where tiltu = #{farmId}")
    @Results({
            @Result(property = "gid", column = "gid"),
            @Result(property = "ely", column = "ely"),
            @Result(property = "knro", column = "knro"),
            @Result(property = "tiltu", column = "tiltu"),
            @Result(property = "lohko", column = "lohko"),
            @Result(property = "pintaAla", column = "pinta_ala"),
            @Result(property = "ymparys", column = "ymparys"),
            @Result(property = "geoJson", column = "geojson")
    })
    List<Peltolohko> findAllByFarmId(String farmId);
}
