package fi.nls.oskari.spring;

import fi.nls.oskari.log.LogFactory;
import fi.nls.oskari.log.Logger;
import fi.nls.oskari.servlet.WebappHelper;
import fi.nls.oskari.util.DuplicateException;
import fi.nls.oskari.util.PropertyUtil;
import org.springframework.security.access.method.P;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Properties;

/**
 * Programmatic initialization of webapp ("web.xml")
 */
public class SpringInitializer implements WebApplicationInitializer {

    private Logger log = LogFactory.getLogger(SpringInitializer.class);

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        // IMPORTANT! read properties at startup - needed for profile selection
        WebappHelper.loadProperties();
        initCustomProperties();
        // re-init logger so we get the one configured in properties
        log = LogFactory.getLogger(SpringInitializer.class);
        final WebApplicationContext context = getContext();
        servletContext.addListener(new ContextLoaderListener(context));
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");
        dispatcher.setAsyncSupported(true);
    }

    public void initCustomProperties() {
        // Allow setting of properties from environment variables. Env vars are prefixed with oskari.
        // oskari.db.username=oskari -> db.username=oskari
        // oskari.oskari.client.version=dist/1.0.0 -> oskari.client.version=dist/1.0.0
        System.out.println("init custom");
        Properties custom = new Properties();
        System.getenv().forEach((key, value) -> {
            if (key.startsWith("oskari__")) {
                String[] splitKeyParts = key.split("oskari__", 2);
                if (splitKeyParts.length > 1) {
                    String newKey = splitKeyParts[1].replace("__", ".");
                    System.out.println("replace " + newKey + " with " + value);
                    custom.put(newKey, value);
                }
            }
        });

        try {
            PropertyUtil.addProperties(custom, true);
        } catch (DuplicateException ignored) {
        }
    }

    private AnnotationConfigWebApplicationContext getContext() {
        final AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.getEnvironment().setDefaultProfiles(SpringEnvHelper.PROFILE_LOGIN_DB);
        final String[] configuredProfiles = PropertyUtil.getCommaSeparatedList("oskari.profiles");
        if (configuredProfiles.length > 0) {
            log.info("Using profiles:", configuredProfiles);
            context.getEnvironment().setActiveProfiles(configuredProfiles);
        }
        context.setConfigLocation("fi.nls.oskari.spring");
        return context;
    }

}
