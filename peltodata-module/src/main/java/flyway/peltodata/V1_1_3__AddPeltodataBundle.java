package flyway.peltodata;

import fi.nls.oskari.domain.map.view.Bundle;
import org.flywaydb.core.api.migration.BaseJavaMigration;
import org.flywaydb.core.api.migration.Context;
import org.oskari.helpers.BundleHelper;

public class V1_1_3__AddPeltodataBundle extends BaseJavaMigration {
    private static final String BUNDLE_ID = "peltodata";

    @Override
    public void migrate(Context context) throws Exception {
        // BundleHelper checks if these bundles are already registered
        Bundle bundle = new Bundle();
        bundle.setName(BUNDLE_ID);
        BundleHelper.registerBundle(context.getConnection(), bundle);
    }
}


